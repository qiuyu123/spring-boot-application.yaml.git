package com.mavenprojectdemo.controller;


import com.mavenprojectdemo.bean.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping(value ="/user")
public class UserController {
    @RequestMapping(value = "index")
    public  Object  index(){
        Map<String,Object>map=new HashMap<>();
        map.put("msg","部署成功maven");
        return map;
    }

    @RequestMapping(value = "/getuserinfo")
    public Object  getUserInfo(){
        User user=new User();
        user.setId(1);
        user.setUsername("徐庆");
        user.setPassword("123456");
        user.setSex("男");
        Map<String,Object>map=new HashMap<>();
        map.put("msg","返回user成功");
        map.put("code",200);
        map.put("user",user);
        return  map;
    }


}
